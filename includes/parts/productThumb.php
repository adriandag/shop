<!-- afisare product thumb -->

<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                <a href="product.php?id=<?php echo $product->id; ?>"> <img
                            src="images/shop/<?php echo $product->getProductImages()[0]->url; ?>" alt=""/></a>
                <h2><?php echo $product->getFinalPrice(); ?></h2>
                <a href="product.php?id=<?php echo $product->id; ?>"><p><?php echo $product->name; ?></p></a>
                <a href="add_to_cart.php?product_id=<?php echo $product-> id;?>&quantity=1" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>

            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div>