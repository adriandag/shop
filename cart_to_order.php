<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 8/23/2019
 * Time: 3:12 AM
 */
include "includes/functions.php";
session_start();

$cart = new Cart($_SESSION['cart_id']);
$order = new Order();

$order->cart_id = $cart->getId();
$order->user_id = $cart->user_id;
$order->user_first_name = $cart->getCustomer()->firstname;
$order->user_last_name = $cart->getCustomer()->lastname;
$order->state = $cart->getCustomer()->state;
$order->city = $cart->getCustomer()->city;
$order->street = $cart->getCustomer()->street;
$order->nr = $cart->getCustomer()->nr;
$order->phone = $cart->getCustomer()->phone;
$order->payment_method = $cart->payment_method;
$order->date = date("YMD");
$order->status = 'xxx';
$order->save();


foreach ($cart->getCartItems() as $cartItem) {

    $orderItem = new OrderItem();
    $orderItem->order_id = $order->getId();
    $orderItem->product_id = $cartItem->product_id;
    $orderItem->product_name = $cartItem->getProduct()->name;
    $orderItem->quantity = $cartItem->quantity;
    $orderItem->price = $cartItem->getProduct()->getFinalPrice();
    $orderItem->save();

}
//unset($_SESSION['cart_id']);
header("Location: checkout.php?order_id=".$order->getId());