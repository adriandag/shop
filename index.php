<!DOCTYPE html>
 <?php include "includes/parts/header.php";
include "includes/functions.php";
?>

<body>
<div class="container">
    <div class="row">
        <?php include "includes/parts/sidebar.php"
        ?>
        <!--/category-products / sidebar-->

        <!--price-range button -->

        <!--products wiew-->
        <div class="col-sm-9 padding-right">
            <div class="features_items"><!--features_items-->
                <h2 class="title text-center">Features Items</h2>
                <!-- afisare produs-->
                <?php
                $limit = 9;
                if (isset($_GET["page"])) {
                    $pn = $_GET["page"];
                } else {
                    $pn = 1;
                };
                $start_from = ($pn - 1) * $limit;

                $productData = dbSelect('product', [], [], $start_from, $limit, ' showOnHomepage', ' DESC');
                foreach ($productData as $productItem) {
                    $product = new Product($productItem['id']);
                    include "includes/parts/productThumb.php"
                    ?>
                    <?php
                }
                ?>
                <ul class="pagination">
                    <?php $total_records = count(dbSelect('product'));
                    $total_pages = ceil($total_records / $limit);
                    $pagLink = "";
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($i == $pn)
                            for ($i = 1; $i <= $total_pages; $i++) {
                                if ($i == $pn)
                                    $pagLink .= "<li class='active'><a href='index.php?page= 
                                    " . $i . "'>" . $i . "</a></li>";
                                else
                                    $pagLink .= "<li><a href='index.php?page=" . $i . "'> 
                                        " . $i . "</a></li>";
                            };
                        echo $pagLink;
                    }
                    ?>
                </ul>
            </div><!--features_items-->
        </div>
    </div>
</div>
</body>

<?php include "includes/parts/footer.php"
?>
</html>