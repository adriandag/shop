<?php

/**
 * Created by PhpStorm.
 * User: dell
 * Date: 8/28/2019
 * Time: 11:10 PM
 */
class customer extends BaseEntity
{
    public $user_id;
    public $firstname;
    public $lastname;
    public $state;
    public $phone;
    public $city;
    public $street;
    public $nr;
    public $email;

    public function getUser()
    {
        return new User($this->user_id);
    }

}