<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/3/2019
 * Time: 1:31 PM
 */
class Product extends BaseEntity
{

    public $name;

    public $description;

    public $price = 0;

    public $discount = 0;

    public $category_id;




    public function getProductImages()
    {
        $data = dbSelect('product_images',['product_id'=>$this->id]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new ProductImage($productData['id']);
        }
        return $result;
    }

public function getComments(){
    $data = dbSelect('comments',['product_id'=>$this->id]);
    $result = [];
    foreach ($data as $productData){
        $result[]=new Comment($productData['id']);
    }
    return $result;
}


    public function getFinalPrice(){
        return $this->price - $this->price*$this->discount;
    }

    public function getCategory(){
        return new Category($this->category_id);
    }

    public function prodSel ($filters)
    {
        $data = dbSelect('product', $filters);
        $result = [];
        foreach ($data as $productData) {
            $result[] = new Product($productData['id']);
        }
        return $result;
    }


}

