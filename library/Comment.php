<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/7/2019
 * Time: 10:35 PM
 */
class Comment extends BaseEntity
{

    public $nickname;
    public $email;
    public $content;
    public $status;
    public $product_id;
    public $date;
    public $time;


    public function getTable()
    {
        return "comments";
    }
}