<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/5/2019
 * Time: 8:45 AM
 */
class ProductImage extends BaseEntity
{

    public $url;
    public $product_id;

    public function getTable()
    {
        return "product_images";
    }
}