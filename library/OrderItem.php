<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/12/2019
 * Time: 11:29 AM
 */
class OrderItem extends BaseEntity
{
    public $order_id;

    public $product_id;

    public $product_name;

    public $quantity;

    public $price;

    public function getTable()
    {
        return 'order_item';
    }

    public function getProduct()
    {
        return new Product($this->product_id);
    }

    public function getOrder()
    {
        return new Order($this->order_id);
    }

    public function getTotal(){
        if ($this->quantity>10){
            return $this->quantity * $this->getProduct()->getFinalPrice()*0.9;
        } else {
            return $this->quantity * $this->getProduct()->getFinalPrice();
        }
    }


}