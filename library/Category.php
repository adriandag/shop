<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/5/2019
 * Time: 8:18 AM
 */
class Category extends BaseEntity
{
    public $id;

    public $name;

    public $parent_id;



    public function getProducts()
    {
        $data = dbSelect('product',['category_id'=>$this->id]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Product($productData['id']);
        }

        return $result;
    }
}

