<?php
include "includes/functions.php";
session_start();
$order = new Order($_GET['order_id']);
$order ->emptyOrder();

$cart = new Cart($_SESSION['cart_id']);
$cart->emptyCart();
unset($_SESSION['cart_id']);

header("Location:index.php");